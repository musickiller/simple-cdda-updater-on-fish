#!/bin/fish
#
# This is a CDDA updater that currently requires version to be setup manually from
# https://cataclysmdda.org/experimental/

set cata_version "2023-07-21-0525"
set cata_type cdda-linux-tiles-sounds-x64
# set cata_type cdda-linux-curses-x64
set folders_to_persist config save templates
set game_folder "cataclysmdda-0.F"
set backup_folder backup

set cata_archive $cata_type-$cata_version.tar.gz
set link_body https://github.com/CleverRaven/Cataclysm-DDA/releases/download
set link "$link_body/cdda-experimental-$cata_version/$cata_archive"

function main
    if test $cata_version = ( cat current_version.txt )
        echo already at selected version
        echo please go to https://cataclysmdda.org/experimental/ and edit this file
        return
    end
    backup
    and download
    and unpack
    and restore_user_stuff
    and cleanup
end

function download
    echo "downloading from $link"
    wget $link
    echo ===== done =====
end

function unpack
    echo unpacking from $cata_archive
    tar -xf $cata_archive
    and rm $cata_archive
    and echo $cata_version > current_version.txt
    echo ===== done =====
end

function cleanup
    echo "removing $backup_folder and $cata_archive"
    rm -rf $backup_folder
    rm -f $cata_archive
    echo ===== done =====
end

function backup
    echo backing up to $backup_folder
    mkdir $backup_folder
    
    for folder in $folders_to_persist
        mv $game_folder/$folder $backup_folder/$folder
    end
    
    mv $game_folder $backup_folder/$game_folder
    echo ===== done =====
end

function restore_user_stuff
    echo restoring user stuff from $backup_folder
    for folder in $folders_to_persist
        mv $backup_folder/$folder $game_folder/$folder
    end
    echo ===== done =====
end

main
